<?php
use Slim\Http\Request;
use Slim\Http\Response; 

//buat dashboard kecamatan

   $app->post("/Wilayah/getTotal/kecamatan/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
 
    $KecamatanID = $params['KecamatanID'];
    try{
       
    $sql = "select NamaKecamatan,
    isnull(sum(Lembaga),0) as TotalLembaga, isnull(sum(singkronSatu),0) as JmlSdh, 
    isnull(sum(singkronNol),0) as JmlBlm, isnull(sum(PTK),0) as JmlPTK, isnull(sum(Rombel),0) as JmlRombel, 
    isnull(sum(anak),0) as JmlAnak 
from (
           select  g.NamaKecamatan,
                  count(a.NPSN) AS Lembaga ,
                  (select count(e.Npsn)  from PAUD_MsHeaderLembaga e where e.StatusSingkron=1 and e.Npsn = a.Npsn) as singkronSatu,
                  (select count(f.Npsn)  from PAUD_MsHeaderLembaga f where f.StatusSingkron=0 and f.Npsn = a.Npsn) as singkronNol ,
                  (select count(p.PtkID) from PAUD_MsHeaderPTK p where p.PaudID = a.Npsn ) as PTK,
                  (select count(b.RombelID)  from PAUD_MSRombel b where b.PaudID = a.Npsn) as Rombel,
                  (select count(c.AnakID)  from PAUD_MSHeaderAnak c where c.PaudID = a.Npsn ) as Anak
           from PAUD_MsHeaderLembaga a, PAUD_Kelurahan d, PAUD_Kecamatan g
           where a.KelurahanID = d.KelurahanID
             and d.KecamatanID = g.KecamatanID
             and g.kecamatanID = :KecamatanID
          group by Npsn,d.NamaKelurahan, g.NamaKecamatan
          ) as lembagaTable
group by NamaKecamatan";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(":KecamatanID", $KecamatanID);
 
    
    $stmt->execute();
        
      
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
    if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});

//buat dashboard kelurahan
$app->post("/Wilayah/getTotal/kelurahan/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
 
    $KelurahanID = $params['KelurahanID'];
    try{
       
    $sql = "select NamaKelurahan NamaKecamatan,NamaKecamatan NamaKecamatan2,
    
    isnull(sum(Lembaga),0) as TotalLembaga, isnull(sum(singkronSatu),0) as JmlSdh, 
    isnull(sum(singkronNol),0) as JmlBlm, isnull(sum(PTK),0) as JmlPTK, isnull(sum(Rombel),0) as JmlRombel, 
    isnull(sum(anak),0) as JmlAnak
from (
           select d.NamaKelurahan, g.NamaKecamatan,
                  count(a.NPSN) AS Lembaga ,
                  (select count(e.Npsn)  from PAUD_MsHeaderLembaga e where e.StatusSingkron=1 and e.Npsn = a.Npsn) as singkronSatu,
                  (select count(f.Npsn)  from PAUD_MsHeaderLembaga f where f.StatusSingkron=0 and f.Npsn = a.Npsn) as singkronNol ,
                  (select count(p.PtkID) from PAUD_MsHeaderPTK p where p.PaudID = a.Npsn ) as PTK,
                  (select count(b.RombelID)  from PAUD_MSRombel b where b.PaudID = a.Npsn) as Rombel,
                  (select count(c.AnakID)  from PAUD_MSHeaderAnak c where c.PaudID = a.Npsn ) as Anak
           from PAUD_MsHeaderLembaga a, PAUD_Kelurahan d, PAUD_Kecamatan g
           where a.KelurahanID = d.KelurahanID
             and d.KecamatanID = g.KecamatanID
             and d.KelurahanID = :KelurahanID
          group by Npsn,d.NamaKelurahan, g.NamaKecamatan
          ) as lembagaTable
group by NamaKelurahan,NamaKecamatan";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(":KelurahanID", $KelurahanID);
 
    
    $stmt->execute();
        
      
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
    if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});
//getlist lembaga yang udah singkron per kelurahan
$app->post("/Wilayah/headerLembagaNonSingkron/ByKelurahan/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    
    $KelurahanID = $params['KelurahanID'];
    try{
       
    $sql = "select a.NamaKelurahan as Kelurahan,z.NamaKecamatan as Kecamatan,l.*  from PAUD_MsHeaderLembaga l join 
    PAUD_Kelurahan  a on l.KelurahanID=a.KelurahanID 
join PAUD_Kecamatan  z on z.KecamatanID = a.KecamatanID
where l.StatusSingkron='0' and  a.KelurahanID=:KelurahanID ";
    $stmt = $this->db->prepare($sql);
   
    $stmt->bindParam(":KelurahanID", $KelurahanID);
 

    $stmt->execute();
        
      
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});

$app->post("/Wilayah/headerLembagaSingkron/ByKelurahan/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    
    $KelurahanID = $params['KelurahanID'];
    try{
       
    $sql = "select a.NamaKelurahan as Kelurahan,z.NamaKecamatan as Kecamatan,l.*  from PAUD_MsHeaderLembaga l join 
    PAUD_Kelurahan  a on l.KelurahanID=a.KelurahanID 
join PAUD_Kecamatan  z on z.KecamatanID = a.KecamatanID
where l.StatusSingkron='1' and  a.KelurahanID=:KelurahanID ";
    $stmt = $this->db->prepare($sql);
   
    $stmt->bindParam(":KelurahanID", $KelurahanID);
 
    $stmt->execute();
        
      
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});

//getlist lembaga yang udah singkron per kecamatan
$app->post("/Wilayah/headerLembagaNonSingkron/ByKecamatan/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $KecamatanID = $params['KecamatanID'];
    try{
       
    $sql = "select a.NamaKelurahan as Kelurahan,z.NamaKecamatan as Kecamatan,l.*  from PAUD_MsHeaderLembaga l join 
    PAUD_Kelurahan  a on l.KelurahanID=a.KelurahanID 
join PAUD_Kecamatan  z on z.KecamatanID = a.KecamatanID
where l.StatusSingkron='0' and  a.KecamatanID=:KecamatanID
order BY a.KelurahanID  ";
    $stmt = $this->db->prepare($sql);
   
    $stmt->bindParam(":KecamatanID", $KecamatanID);
 

    $stmt->execute();
        
      
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});
$app->post("/Wilayah/headerLembagaSingkron/ByKecamatan/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $KecamatanID = $params['KecamatanID'];
   
    try{
       
    $sql = "select a.NamaKelurahan as Kelurahan,z.NamaKecamatan as Kecamatan,l.*  from PAUD_MsHeaderLembaga l join 
    PAUD_Kelurahan  a on l.KelurahanID=a.KelurahanID 
join PAUD_Kecamatan  z on z.KecamatanID = a.KecamatanID
where l.StatusSingkron='1' and  a.KecamatanID=:KecamatanID
order BY a.KelurahanID  ";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(":KecamatanID", $KecamatanID);
   
    $stmt->execute();
        
      
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});
