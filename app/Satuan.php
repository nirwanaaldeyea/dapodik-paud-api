<?php
use Slim\Http\Request;
use Slim\Http\Response; 


 $app->post("/Satuan/Dashboard/ByLembaga/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $Npsn = $params['Npsn'];
    try{
    $sql = "select	a.Npsn,d.NamaKelurahan, g.NamaKecamatan,a.NamaKetua,a.NamaPaud,a.DayaTampung,a.LastSingkron,a.Tahun,CASE
    WHEN a.Semester = 1 THEN 'Ganjil'
    WHEN a.Semester= 2 THEN 'Genap'
    ELSE '-'
END AS Semester1,
CASE
    WHEN a.StatusSingkron = 1 THEN 'Sudah Singkron'
    WHEN a.StatusSingkron = 0 THEN 'Belum Singkron'
    ELSE '-'
END AS StatusSingkron1,
CASE
    WHEN a.IsActive = 1 THEN 'Aktif'
    WHEN  a.IsActive = 0 THEN 'Tidak Aktif'

    ELSE '-'
END AS StatusAktif,
		(select count(p.PtkID) from PAUD_MsHeaderPTK p where p.PaudID = a.Npsn ) as TotalPtk,
		(select count(b.RombelID)  from PAUD_MSRombel b where b.PaudID = a.Npsn) as TotalRombel,
		(select count(c.AnakID)  from PAUD_MSHeaderAnak c where c.PaudID = a.Npsn ) as TotalAnak
from PAUD_MsHeaderLembaga a,PAUD_Kelurahan d, PAUD_Kecamatan g
where a.KelurahanID = d.KelurahanID
  and d.KecamatanID = g.KecamatanID
  and a.Npsn =:Npsn
group by a.Npsn,d.NamaKelurahan, g.NamaKecamatan, Npsn,a.NamaKetua,a.NamaPaud,a.IsActive,a.StatusSingkron,a.DayaTampung,a.LastSingkron,a.Semester,a.Tahun
";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(":Npsn", $Npsn );
 

    $stmt->execute();
        
      
        $result = $stmt->fetchAll();
        $count = $stmt->rowCount();
    if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data INFO HEADER LEMBAGA Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});
$app->post("/Satuan/headerLembaga/ByLembaga/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $Npsn = $params['Npsn'];
    
    try{
      
    $sql = "select k.NamaKelurahan as Kelurahan,z.NamaKecamatan, CASE
    WHEN a.StatusSingkron = 1 THEN 'Sudah Singkron'
    WHEN a.StatusSingkron = 2 THEN 'Belum Singkron'
    ELSE 'APPROVED'
END AS StatusSingkron, a.* from PAUD_MsHeaderLembaga a join 
PAUD_Kelurahan k on a.KelurahanID=k.KelurahanID 
join PAUD_Kecamatan  z on z.KecamatanID = k.KecamatanID WHERE a.NPSN=:Npsn";
    $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":Npsn", $Npsn);
     
        $stmt->execute();
        
      
        $result = $stmt->fetchAll();
        $count = $stmt->rowCount();
    if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Header Lembaga Kosong";
    }
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});
$app->post("/Satuan/detailLembaga/ByLembaga/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $Npsn = $params['Npsn'];
    try{
       
    $sql = "select b.* from PAUD_MsHeaderLembaga a Join  PAUD_MsDetailLembaga b on b.NPSN=a.Npsn WHERE a.NPSN=:Npsn";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(":Npsn", $Npsn );
 
    $stmt->execute();
        
      
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Header Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});
$app->post("/Satuan/yayasanLembaga/ByLembaga/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $Npsn = $params['Npsn'];
    try{
        
    $sql = "select * from PAUD_MsLembagaYayasan a join 
    PAUD_MsHeaderLembaga b on  b.Npsn=a.PaudID 
    WHERE b.Npsn=:Npsn";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(":Npsn", $Npsn );
 
    $stmt->execute();
        
      
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Header Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});
$app->post('/paud/prasarana/add/', function ($request, $response) { 
    try{
        $con = $this->db;

        $sql = "INSERT INTO PAUD_MsLembagaPrasarana (PaudID,NamaPrasarana,JenisPrasarana,Panjang,Lebar,Keterangan) VALUES (:PaudID,:NamaPrasarana,:JenisPrasarana,:Panjang,:Lebar,:Keterangan)";
        $pre  = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $values = array(
        
        ':PaudID' => $request->getParam('MomID'),
       
        ':NamaPrasarana' => $request->getParam('NamaPrasarana'),
       
        ':JenisPrasarana' => $request->getParam('JenisPrasarana'),
        ':Panjang' => $request->getParam('Panjang'),
        ':Lebar' => $request->getParam('Lebar'),
        ':Keterangan' => $request->getParam('Keterangan')
        );
        $result = $pre->execute($values);
       
        // $count = $result->rowCount();
        if($result){
            
            $data2['msg'] = "Success Add Data Prasarana";    
            $data2['out'] = 1;
            
        }else{
            $data2['out'] = 0;
            $data2['msg'] = "Error: Add Prasarana";
        
        }
        
    } catch (PDOException $e) {
        $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
        $data['out'] = 0;
        $data['msg'] = "Error: ". $e->getMessage();
    } catch (Exception $e) {
        $this['logger']->error("General Error.<br/>" . $e->getMessage());
        $data['out'] = 0;
        $data['msg'] = "Error: ". $e->getMessage();
    } finally {
        // Destroy the database connection
        $this->db = null;
    }
    // Return the result
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data2);
    return $response;
    });
$app->post("/Satuan/prasaranaLembaga/ByLembaga/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $Npsn = $params['Npsn'];
    try{
        
    $sql = "select * from PAUD_MsLembagaPrasarana a join 
    PAUD_MsHeaderLembaga b on  b.Npsn=a.PaudID 
    WHERE b.Npsn=:Npsn";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(":Npsn", $Npsn );
    $stmt->execute();
        $result = $stmt->fetchAll();
        $count = $stmt->rowCount();
    if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Header Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});

$app->post("/Satuan/rombel/ByLembaga/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $Npsn = $params['Npsn'];
    try{
        
    $sql = "select h.NamaPtk,P.NamaPrasarana,a.* from PAUD_MSRombel a join 
    PAUD_MsHeaderLembaga b on  b.Npsn=a.PaudID  left join PAUD_MsHeaderPTK h on h.PtkID=a.PtkID  left join PAUD_MsLembagaPrasarana P on P.PrasaranaID=a.PrasaranaID
     WHERE b.Npsn=:Npsn";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(":Npsn", $Npsn );
 
    $stmt->execute();
        
      
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Header Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});
$app->post("/Satuan/anak/ByRombel/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $RombelID= $params['RombelID'];
    try{
        
    $sql = "SELECT a.* ,CASE
    WHEN a.JenisKelamin = 1 THEN 'Laki-Laki'
    WHEN a.JenisKelamin  = 2 THEN 'Perempuan'
    ELSE '-'
END AS JK,
CASE
    WHEN a.IsActive = 1 THEN 'Aktif'
    WHEN a.IsActive  = 0 THEN 'Tidak Aktif'
    ELSE '-'
END AS StatusAnak FROM PAUD_MSHeaderAnak a WHERE a.RombelID=:RombelID";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(":RombelID", $RombelID );
 
    $stmt->execute();
        
      
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Header Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});


    $app->post("/Satuan/alumni/ByLembaga/", function (Request $request, Response $response){
        $params = $request->getParsedBody();
        $PaudID = $params['PaudID'];
        try{
            $sql ="select CASE
            WHEN a.JenisKelamin = 1 THEN 'Laki-Laki'
            WHEN a.JenisKelamin  = 2 THEN 'Perempuan'
            ELSE '-'
        END AS JK,
CASE
        WHEN b.JenisMasuk = 1 THEN 'Baru'
        WHEN b.JenisMasuk  = 2 THEN 'Pindahan'
        ELSE '-'
    END AS JenisMasuk1,
    CASE
        WHEN b.AlasanKeluar = 1 THEN 'Lulus'
        WHEN b.AlasanKeluar = 2 THEN 'Pindah/Keluar'
        ELSE '-'
    END AS AlasanKeluar1,a.*,b.* from PAUD_MSHeaderAnak a join PAUD_MsAnakRegistrasi b on 
    a.AnakID=b.AnakID WHERE a.IsActive=0 and a.PaudID=:PaudID ORDER BY b.TglKeluar ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":PaudID", $PaudID );
        $stmt->execute();
            
          
        $result = $stmt->fetchAll();
        $count = $stmt->rowCount();
    if ($count != 0) {
            $data['data'] = $result;
            $data['msg'] = "Sukses";
            $data['out'] = 1;
            $data['jml'] = $count;
        } else {
            // data wrong
            $data['out'] = 0;
            $data['msg'] = "Error: Data Header Lembaga Kosong";
        }
     
        $response = $response->withHeader('Content-Type', 'application/json');
        $response = $response->withStatus(200);
        $response = $response->withJson($data);
        return $response;
    } catch (PDOException $e) {
        $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
    } catch (Exception $e) {
        $this['logger']->error("General Error.<br/>" . $e->getMessage());
    } finally {
        // Destroy the database connection
      
    }
    });
    $app->post("/Satuan/headerAnak/ByPaud/", function (Request $request, Response $response){
        $params = $request->getParsedBody();
        $PaudID = $params['PaudID'];
        try{
            $sql ="select CASE
            WHEN a.JenisKelamin = 1 THEN 'Laki-Laki'
            WHEN a.JenisKelamin  = 2 THEN 'Perempuan'
            ELSE '-'
        END AS JK,
        CASE
            WHEN a.IsActive = 1 THEN 'Aktif'
            WHEN a.IsActive  = 0 THEN 'Tidak Aktif'
            ELSE '-'
        END AS StatusAnak,a.* from PAUD_MSHeaderAnak a WHERE a.isActive='1' and
 a.PaudID=:PaudID";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":PaudID", $PaudID );
     
    
        $stmt->execute();
            
          
        $result = $stmt->fetchAll();
        $count = $stmt->rowCount();
    if ($count != 0) {
            $data['data'] = $result;
            $data['msg'] = "Sukses";
            $data['out'] = 1;
            $data['jml'] = $count;
        } else {
            // data wrong
            $data['out'] = 0;
            $data['msg'] = "Error: Data Header Lembaga Kosong";
        }
     
        $response = $response->withHeader('Content-Type', 'application/json');
        $response = $response->withStatus(200);
        $response = $response->withJson($data);
        return $response;
    } catch (PDOException $e) {
        $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
    } catch (Exception $e) {
        $this['logger']->error("General Error.<br/>" . $e->getMessage());
    } finally {
        // Destroy the database connection
      
    }
    });
    
    
$app->post("/Satuan/headerAnak/ByAnak/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $AnakID = $params['AnakID'];
    try{
        $sql ="select R.NamaRombel Rombel1,CASE
        WHEN a.JenisKelamin = 1 THEN 'Laki-Laki'
        WHEN a.JenisKelamin  = 2 THEN 'Perempuan'
        ELSE '-'
    END AS JK,
    CASE
        WHEN a.IsActive = 1 THEN 'Aktif'
        WHEN a.IsActive  = 0 THEN 'Tidak Aktif'
        ELSE '-'
    END AS StatusAnak,a.* from PAUD_MSHeaderAnak a LEFT JOIN PAUD_MSRombel R ON R.RombelID=a.RombelID WHERE AnakID=:AnakID";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(":AnakID", $AnakID );
 

    $stmt->execute();
        
      
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Header Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});

$app->post("/Satuan/KompetensiPTK/ByPTK/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $PtkID = $params['PtkID'];
    try{
        $sql ="select * from PAUD_KompetensiPTK a join 
        PAUD_MsHeaderPTK b on  b.PtkID=a.PtkID  WHERE a.PtkID=:PtkID";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(":PtkID", $PtkID );
 

    $stmt->execute();
        
      
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Header Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});




$app->post("/Satuan/detailAnak/ByAnak/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $AnakID = $params['AnakID'];
    try{
        $sql ="select CASE
        WHEN a.Agama = 1 THEN 'Islam'
        WHEN a.Agama = 2 THEN 'Kristen'
        ELSE 'Lainnya'
    END AS AgamaAnak,
    CASE
        WHEN a.TempatTinggal = 1 THEN 'Bersama Orang Tua'
        WHEN a.TempatTinggal = 2 THEN 'Tidak Bersama Orang Tua'
        ELSE 'APPROVED'
    END AS Tinggal,
    CASE
        WHEN a.ModaTransportasi = 1 THEN 'Jalan Kaki'
        WHEN a.ModaTransportasi = 2 THEN 'Kendaraan'
        ELSE 'APPROVED'
    END AS Transpport,
    a.* from PAUD_MSDetailAnak a join 
            PAUD_MSHeaderAnak b on  b.AnakID=a.AnakID  WHERE a.AnakID=:AnakID";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(":AnakID", $AnakID );
 

    $stmt->execute();
        
      
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Header Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});

$app->post("/Satuan/RegistrasiAnak/ByAnak/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $AnakID = $params['AnakID'];
    try{
        $sql ="select  CASE
        WHEN a.JenisMasuk = 1 THEN 'Baru'
        WHEN a.JenisMasuk  = 2 THEN 'Pindahan'
        ELSE '-'
    END AS JenisMasuk1,
    CASE
        WHEN a.AlasanKeluar = 1 THEN 'Lulus'
        WHEN a.AlasanKeluar = 2 THEN 'Pindah'
        ELSE '-'
    END AS AlasanKeluar,a.* from PAUD_MsAnakRegistrasi a join 
        
        PAUD_MSHeaderAnak b on  b.AnakID=a.AnakID WHERE  a.AnakID=:AnakID";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(":AnakID", $AnakID );
 

    $stmt->execute();
        
      
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Header Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});


$app->post("/Satuan/OrtuAnak/ByAnak/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $AnakID = $params['AnakID'];
    try{
        $sql ="select CASE
        WHEN b.JenisKelamin = 1 THEN 'Laki-Laki'
        WHEN b.JenisKelamin  = 2 THEN 'Perempuan'
        ELSE '-'
    END AS JK,a.* from PAUD_MsOrtu a join 

        PAUD_MSHeaderAnak b on  b.AnakID=a.AnakID  WHERE a.AnakID=:AnakID";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(":AnakID", $AnakID );
 

    $stmt->execute();
        
      
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Header Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});

$app->post("/Satuan/HeaderPTK/ByPaud/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $PaudID = $params['PaudID'];
    try{
        $sql ="select s.StatusPegawai as kepegawaian, j.NamaJabatan,a.* from PAUD_MsHeaderPTK a 
        join PAUD_MsJabatan j on j.JabatanID=a.Jabatan 
        join PAUD_MsStatusKepegawaian s on s.StatusPegawaiID=a.StatusKepegawaian
        WHERE a.PaudID=:PaudID";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(":PaudID", $PaudID );
 

    $stmt->execute();
        
      
        $result = $stmt->fetchAll();
        $count = $stmt->rowCount();
    if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Header Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});

$app->post("/Satuan/HeaderPTK/ByPTK/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $PtkID = $params['PtkID'];
    try{
        $sql ="select s.StatusPegawai as kepegawaian, j.NamaJabatan,a.* from PAUD_MsHeaderPTK a 
        join PAUD_MsJabatan j on j.JabatanID=a.Jabatan 
        join PAUD_MsStatusKepegawaian s on s.StatusPegawaiID=a.StatusKepegawaian
        WHERE a.PtkID=:PtkID";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(":PtkID", $PtkID );
 

    $stmt->execute();
        
      
        $result = $stmt->fetchAll();
        $count = $stmt->rowCount();
    if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Header Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});

$app->post("/Satuan/KepegawaianPTK/ByPTK/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $PtkID = $params['PtkID'];
    try{
        $sql ="select * from PAUD_MsKepegawaianPTK a join 
        PAUD_MsHeaderPTK b on  b.PtkID=a.PtkID WHERE a.PtkID=:PtkID";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(":PtkID", $PtkID );
 

    $stmt->execute();
        
      
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Header Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});

$app->post("/Satuan/DetailPTK/ByPTK/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $PtkID = $params['PtkID'];
    try{
        $sql ="select  CASE
        WHEN a.JenisKelamin = 1 THEN 'Laki-Laki'
        WHEN a.JenisKelamin  = 2 THEN 'Perempuan'
        ELSE '-'
    END AS JK,
 CASE
        WHEN a.StatusKawin = 0 THEN 'Belum Kawin'
        WHEN a.StatusKawin  = 1 THEN 'Sudah Kawin'
        ELSE 'Cerai'
    END AS kawin,
CASE
        WHEN a.Agama = 1 THEN 'Islam'
        WHEN a.Agama  = 2 THEN 'Kristen'
        ELSE 'Lainnya'
    END AS agama1,a.* from PAUD_MsDetailPTK a WHERE a.PtkID=:PtkID";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(":PtkID", $PtkID );
 

    $stmt->execute();
        
      
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Header Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});


$app->post("/Satuan/PeriodikAnak/ByAnak/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $AnakID = $params['AnakID'];
    try{
        $sql ="select * from PAUD_PeriodikAnak a join 
        PAUD_MSHeaderAnak b on  b.AnakID=a.AnakID  WHERE a.AnakID=:AnakID";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam(":AnakID", $AnakID );
 

    $stmt->execute();
        
      
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Header Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});

