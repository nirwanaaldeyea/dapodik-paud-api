<?php
use Slim\Http\Request;
use Slim\Http\Response; 

$app->post("/Master/getKecamatan/", function (Request $request, Response $response){
    
    try{
       
    $sql = "select * from PAUD_Kecamatan";
   
    $stmt = $this->db->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();

    if ($result) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});

$app->post("/Master/getKelurahan/", function (Request $request, Response $response){
    
    try{
        $sql = "select * from PAUD_Kelurahan";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $count = $stmt->rowCount();
   
  
    if ($result) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});


$app->post("/Master/getJabatan/", function (Request $request, Response $response){
    
    try{
        $sql = "select * from PAUD_MsJabatan";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $count = $stmt->rowCount();
   
  
    if ($result) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});


$app->post("/Master/getStatusKepegawaian/", function (Request $request, Response $response){
    
    try{
        $sql = "select * from PAUD_MsStatusKepegawaian";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $count = $stmt->rowCount();
   
  
    if ($result) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});