<?php
use Slim\Http\Request;
use Slim\Http\Response; 
// $app->add(function (Request $request, Response $response, $next) {
//     if ($request->getMethod() !== 'OPTIONS' || php_sapi_name() === 'cli') {
//         return $next($request, $response);
//     }

    
// });
$app->get('/hello/', function (Request $request, Response $response){
    return "Hello wordls";
    $response = $next($request, $response);

    $response = $response->withHeader('Access-Control-Allow-Origin', '*');
    $response = $response->withHeader('Access-Control-Allow-Methods', $request->getHeaderLine('Access-Control-Request-Method'));
    $response = $response->withHeader('Access-Control-Allow-Headers', $request->getHeaderLine('Access-Control-Request-Headers'));

    return $response;
});
//dashboard pusat
$app->post("/pusat/getTotal/", function (Request $request, Response $response){
    try{
    $sql = "select count(NPSN) AS TotalLembaga ,
    (select count(NPSN)  from PAUD_MsHeaderLembaga where StatusSingkron=1) as JmlSdh
    ,(select count(NPSN)  from PAUD_MsHeaderLembaga where StatusSingkron=0) as JmlBlm,
(select count(p.PtkID)  from PAUD_MsHeaderPTK p ) as JmlPTK,
(select count(b.RombelID)  from PAUD_MSRombel b ) as JmlRombel,
(select count(c.AnakID)  from PAUD_MSHeaderAnak c ) as JmlAnak
    from PAUD_MsHeaderLembaga
    ";
    $stmt = $this->db->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
    if ($result) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});
//get lembaga yang sudah singkron dan belum di pusat
$app->post("/pusat/getAllHeaderLembaga/", function (Request $request, Response $response){
    try{
    $sql = "select  k.NamaKelurahan as Kelurahan,z.NamaKecamatan as Kecamatan, l.* 
    from PAUD_MsHeaderLembaga l join 
    PAUD_Kelurahan k on l.KelurahanID=k.KelurahanID 
join PAUD_Kecamatan  z on z.KecamatanID = k.KecamatanID
where StatusSingkron='1' 
order by z.KecamatanID ";
    $stmt = $this->db->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
    if ($result) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});
$app->post("/pusat/getAllHeaderLembagaNon/", function (Request $request, Response $response){
    try{
    $sql = "select k.NamaKelurahan as Kelurahan,z.NamaKecamatan as Kecamatan, l.* 
    from PAUD_MsHeaderLembaga l join 
    PAUD_Kelurahan k on l.KelurahanID=k.KelurahanID 
join PAUD_Kecamatan  z on z.KecamatanID = k.KecamatanID
where StatusSingkron='0'
order by z.KecamatanID";
    $stmt = $this->db->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
    if ($result) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});

//ini dipake pas detail lembaga semua
$app->post("/pusat/getheaderLembaga/ByLembaga/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $Npsn = $params['Npsn'];
    
    try{
      
    $sql = "select k.NamaKelurahan as Kelurahan,z.NamaKecamatan, CASE
    WHEN a.StatusSingkron = 1 THEN 'Sudah Singkron'
    WHEN a.StatusSingkron = 2 THEN 'Belum Singkron'
    ELSE 'APPROVED'
END AS StatusSingkron1, a.* from PAUD_MsHeaderLembaga a join 
PAUD_Kelurahan k on a.KelurahanID=k.KelurahanID 
join PAUD_Kecamatan  z on z.KecamatanID = k.KecamatanID WHERE a.NPSN=:Npsn";
    $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":Npsn", $Npsn);
     
        $stmt->execute();
        
      
        $result = $stmt->fetchAll();
        $count = $stmt->rowCount();
    if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Header Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});

$app->post("/pusat/getrombelLembaga/ByLembaga/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $PaudID = $params['PaudID'];
    
    try{
      
    $sql = "select (a.JmlL+a.JmlP) as Total, a.* from PAUD_MSRombel a WHERE a.PaudID=:PaudID";
    $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":PaudID", $PaudID);
     
        $stmt->execute();
        
      
        $result = $stmt->fetchAll();
        $count = $stmt->rowCount();
    if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Header Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});

$app->post("/pusat/getPtkLembaga/ByLembaga/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $PaudID = $params['PaudID'];
    
    try{
      
    $sql = "select s.StatusPegawai as kepegawaian, j.NamaJabatan,a.* from PAUD_MsHeaderPTK a 
    join PAUD_MsJabatan j on j.JabatanID=a.Jabatan 
    join PAUD_MsStatusKepegawaian s on s.StatusPegawaiID=a.StatusKepegawaian
    WHERE a.PaudID=:PaudID";
    $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":PaudID", $PaudID);
     
        $stmt->execute();
        
      
        $result = $stmt->fetchAll();
        $count = $stmt->rowCount();
    if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Header Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});


$app->post("/pusat/getPrasaranaLembaga/ByLembaga/", function (Request $request, Response $response){
    $params = $request->getParsedBody();
    $PaudID = $params['PaudID'];
    
    try{
      
    $sql = "select a.* from PAUD_MsLembagaPrasarana a 
    WHERE a.PaudID=:PaudID";
    $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":PaudID", $PaudID);
     
        $stmt->execute();
        
      
        $result = $stmt->fetchAll();
        $count = $stmt->rowCount();
    if ($count != 0) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data Header Lembaga Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});
