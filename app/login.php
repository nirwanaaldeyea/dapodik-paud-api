<?php
use Slim\Http\Request;
use Slim\Http\Response; 
$app->get("/getHistoryLogin/", function (Request $request, Response $response){
    try{
    $sql = "select * from PAUD_MSUserLogin  ";
    $stmt = $this->db->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetchAll();
    $count = $stmt->rowCount();
    if ($result) {
        $data['data'] = $result;
        $data['msg'] = "Sukses";
        $data['out'] = 1;
        $data['jml'] = $count;
    } else {
        // data wrong
        $data['out'] = 0;
        $data['msg'] = "Error: Data History Login Kosong";
    }
 
    $response = $response->withHeader('Content-Type', 'application/json');
    $response = $response->withStatus(200);
    $response = $response->withJson($data);
    return $response;
} catch (PDOException $e) {
    $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
} catch (Exception $e) {
    $this['logger']->error("General Error.<br/>" . $e->getMessage());
} finally {
    // Destroy the database connection
  
}
});



$app->post('/paud/login/', function (Request $request, Response $response) {
    // Gets username and password
    $params = $request->getParsedBody();
    $Username = $params['Username'];
    $Password = $params['Password'];


    try {
        // Gets the user into the database
        $sql = "select * from PAUD_MsUser where isActive='1' and username=:Username and Passsword=:Password";
        //tambah alamat
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":Username", $Username);
        $stmt->bindParam(":Password", $Password);
      
        $stmt->execute();
        
        $query = $stmt->fetchObject();
        $result = $stmt->fetchAll();
        
       // return $username +:;
        //alert($query);
        // If user exist
   
        $count = $stmt->rowCount();
    
        if($count != 0){
            $Username= ("$query->Username");
            $name= ("$query->FullName");
            $dt = new DateTime();
            $token = ("$query->Username$query->UserID");
            $token2 = $dt->format('YmdHis');
            $token3 = ($token.$token2);
            $sql1 = "insert into PAUD_MSUserLogin( token,tglLogin,tglLogout,username,nama) values ('$token3',getdate(),'','$Username','$name') ";
            $stmt1 = $this->db->prepare($sql1);
            $result1 = $stmt1->execute();
            // Create a new resource
            $data1['Token'] = $token3;
            $data1['Username'] = $query->Username;
            $data1['Jenis'] = $query->JenisUserID;
            $data1['Nama'] = $query->FullName;
            $data1['Wilayah'] = $query->WilayahID;
            $data1['Paud'] = $query->PaudID;
            $data['data'] = $data1;
            $data['msg history'] = "Sukses add history login ";
            $data['msg login '] = "Sukses login ";
            $data['out'] = 1;
        }else{
            
            $data['out'] = 0;
            $data['msg'] = "Error: The user / Pass specified does not exist. ";
        }

      
        
        $response = $response->withHeader('Content-Type', 'application/json');
        $response = $response->withStatus(200);
        $response = $response->withJson($data);
      
        return $response;
    } catch (PDOException $e) {
        $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
    } catch (Exception $e) {
        $this['logger']->error("General Error.<br/>" . $e->getMessage());
    } finally {
        // Destroy the database connection
      
    }
});


// $app->post('/dms/logout/', function (Request $request, Response $response) {
//     // Gets username and password
//     $params = $request->getParsedBody();
//     $Token= $params['Token'];

//     try {

      
//              // Gets the user into the database
//         $sql = "SELECT   UserID UserID,Username Username,Token Token,Login Login ,Ip Ip FROM DMS_HistoryLogin a WHERE Token=:Token";
//         //tambah alamat
//         $stmt = $this->db->prepare($sql);
//         $stmt->bindParam(":Token", $Token);
    
//         $stmt->execute();
//         $query = $stmt->fetchObject();
//         //$result = $stmt->fetchAll();
//        // return $username +:;
//         //alert($query);
//         // If user exist
       
//         $count = $stmt->rowCount();
     
     
//         if($count != 0){
//             $Token= ('$query->Token');
//             $Logout=(getdate());
//             // $Logout=("");
//             $sqlUpdate = "UPDATE DMS_HistoryLogin SET Logout=getdate() WHERE Token=:Token";
//             $update = $this->db->prepare($sqlUpdate);
//             $update->bindParam(":Token", $Token);
            
          

//             $data['msg'] = "Log Out Success ";
//             $data['out'] = 1;
//         } else {
//             // Username wrong
//             $data['out'] = 0;
//             $data['msg'] = "Error: Login does not exist. ";
//         }

//         // Return the result
        
//         $response = $response->withHeader('Content-Type', 'application/json');
//         $response = $response->withStatus(200);
//         $response = $response->withJson($data);
        
//         return $response;
//     } catch (PDOException $e) {
//         $this['logger']->error("DataBase Error.<br/>" . $e->getMessage());
//     } catch (Exception $e) {
//         $this['logger']->error("General Error.<br/>" . $e->getMessage());
//     } finally {
//         // Destroy the database connection
      
//     }
// });
