<?php


    $container = $app->getContainer();

    // view renderer
    $container['renderer'] = function ($c) {
        $settings = $c->get('settings')['renderer'];
        return new \Slim\Views\PhpRenderer($settings['template_path']);
    };

    // monolog
    $container['logger'] = function ($c) {
        $settings = $c->get('settings')['logger'];
        $logger = new \Monolog\Logger($settings['name']);
        $logger->pushProcessor(new \Monolog\Processor\UidProcessor());
        $logger->pushHandler(new \Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
        return $logger;
    };

$container['db'] = function ($c){
    $settings = $c->get('settings')['db'];
    $server = $settings['driver'].":host=".$settings['host'].";dbname=".$settings['dbname'];
    // $conn = new PDO($server, $settings["user"], $settings["pass"]);  
    //$conn = new PDO("sqlsrv:Server=10.7.64.252;Database=engineerre", "sa", "P@ssw0rd");
     // $conn = new PDO("sqlsrv:Server=localhost;Database=engineerre", "sa", "P@ssw0rd");
      
    $conn = new PDO("sqlsrv:Server=localhost;Database=paud", "sa", "P@ssw0rd");
    
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $conn;
};
