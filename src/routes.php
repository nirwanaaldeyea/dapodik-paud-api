<?php

use Slim\Http\Request;
use Slim\Http\Response;

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
                    ->withHeader('Access-Control-Allow-Origin', '*')
                    ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Auth-Token, Content-Type, Accept, Origin, Authorization')
                    ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});
$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

// include "../app/mstCustomer.php";
// include "../app/mstUser.php";
// include "../app/mstDivisi.php";
// include "../app/mstJabatan.php";
// include "../app/mstMenu.php";
// include "../app/mstRole.php";
// include "../app/mstRoleMenu.php";
// include "../app/mstRoleUser.php";

include "../app/login.php";
include "../app/Master.php";
include "../app/Pusat.php";
include "../app/Wilayah.php";
include "../app/Satuan.php";
